class TweetsController < ApplicationController
  before_filter :authenticate_user!,
    :only => [:new, :create, :destroy]
  
  def new
    @tweet = User.find(current_user).tweets.new
  end
  
  def index
    @tweets = Tweet.find(:all, :order => "id DESC", :limit => 10)
    if user_signed_in?
      @user = User.find(current_user)
    end
  end
  
  def show
    if user_signed_in?
      @user = User.find(current_user)
    end
    @tweet = Tweet.find(params[:id])
  end
  
  def create
    @tweet = User.find(current_user).tweets.new(validate_params)
    if @tweet.save
      redirect_to @tweet
    else
      render 'new'
    end
  end
  
  def destroy
    @tweet = User.find(current_user).tweets.find(params[:id])
    @tweet.destroy
    redirect_to tweets_path
  end
  
  private
    def validate_params
      return params.require(:tweet).permit(:text)
    end
  
end
