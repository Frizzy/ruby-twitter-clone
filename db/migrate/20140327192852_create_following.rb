class CreateFollowing < ActiveRecord::Migration
  def change
    create_table :followings do |t|
      t.references :user_id, polymorphic: true, index: true
    end
  end
end
